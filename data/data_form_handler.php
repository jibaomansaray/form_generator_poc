<?php

$data = $_POST;
$fieldId = filter_input(INPUT_POST, 'form_id');

$pathToData = __DIR__ . "/form_{$fieldId}_data.csv";

if (isset($data['form_id'])) {
  unset($data['form_id']);
}

if (!file_exists($pathToData)) {
  $title = "";
  foreach (array_keys($data) as $name) {
    $temp = implode(' ', explode('_', $name));
    $title .= "\"{$temp}\",";
  }
  $title = rtrim($title, ',');
  file_put_contents($pathToData, "{$title}\n");
}

$row = '';
foreach ($data as $name => $value) {
  $row .= "\"{$value}\",";
}
$row = rtrim($row, ',');
file_put_contents($pathToData, "{$row}\n", FILE_APPEND);

print_r($data);
