<?php
$formId = $_GET['form'];
$formPath =  __DIR__ . "/form{$formId}.json";
$definition = [];

if (file_exists($formPath)) {
  $definition = json_decode(file_get_contents($formPath), true);
} else {
  die("Form does not exist");
}

function generateId(array $def): string
{
  return strtolower(implode('_', explode(' ', $def['name'])));
}

function generateAttributeStr(array $def, array $attributes = []): string
{
  $attributes['name'] = $def['name'];
  if (!isset($attributes['id'])) {
    $attributes['id'] = generateId($def);
  }

  if (isset($def['require'])) {
    $attributes['required'] = 1;
  }

  $attr = '';
  foreach ($attributes as $key => $value) {
    $attr .= "{$key}=\"{$value}\"";
  }

  return $attr;
}

function textField(array $def, string $type = 'text'): string
{
  $id = generateId($def);
  $attributes = [
    'type' => $type,
    'id' => $id
  ];
  $attr = generateAttributeStr($def, $attributes);

  return "<label for=\"{$id}\">{$def['name']}<label><br/><input {$attr} />";
}

function numberField(array $def): string  {
  return textField($def, 'number');
}

function textAreaField(array $def): string
{
  $id = generateId($def);
  $attr = generateAttributeStr($def, []);

  return "<label for=\"{$id}\">{$def['name']}<label><br/><textarea {$attr} ></textarea>";
}

function selectField(array $def): string
{
  return '';
}

function multiSelectField(array $def): string
{
  return '';
}

function checkboxField(array $def): string
{
  return '';
}


function generateField(array $fieldDefinition): string
{
  return match ($fieldDefinition['type']) {
    "text" => textField($fieldDefinition),
    "number" => numberField($fieldDefinition),
    "text-area" => textAreaField($fieldDefinition),
    "select" => selectField($fieldDefinition),
    "multi-select" => multiSelectField($fieldDefinition),
    "checkbox" => checkboxField($fieldDefinition),
    default => textField($fieldDefinition)
  };
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
</head>

<body>
  <h1>Data form</h1>
  <form method="POST" action="/data/data_form_handler.php">
    <input type="hidden" value="<?=$formId ?>" name="form_id" />
    <?php
    foreach ($definition['fields'] as $aDefinition) {
      echo generateField($aDefinition) . '<br/>';
    }
    ?>
    <p>
      <input type="submit" value="Send" />
    </p>
  </form>
</body>

</html>