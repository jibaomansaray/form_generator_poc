let fieldCount = 0;
function addNewFieldDefinition() {
  fieldCount++;
  const fieldDefinitionTemplate = `
          <label for="name_${fieldCount}">Name</label>
        <input type="text" id="name_${fieldCount}" name="fields[${fieldCount}][name]" placeholder="Field Name" /><br />
        <label for="type_${fieldCount}">Type</label>
        <select id="type_${fieldCount}" name="fields[${fieldCount}][type]">
          <option value="text">Text</option>
          <option value="text-area">Text Area</option>
          <option value="select">Select</option>
          <option value="multi-select">Multi-Select</option>
          <option value="checkbox">Checkbox</option>
        </select> <br />
        <label for="require_${fieldCount}">Is Require</label>
        <input type="checkbox" id="require_${fieldCount}" name="fields[${fieldCount}][require]" /><br />
  `;
  const element = document.getElementById('fields');

  if (element) {
    const newNode = document.createElement('p');
    newNode.innerHTML = fieldDefinitionTemplate
    element.append(newNode);
  }
}