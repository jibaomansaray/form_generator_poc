<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Form Generator</title>
</head>

<body>
  <h1>Form Generator</h1>
  <form method="POST" action="/generator/post_handler.php">
    <div id="fields">
      <div>
        <label for="name_0">Name</label>
        <input type="text" id="name_0" name="fields[0][name]" placeholder="Field Name" /><br />
        <label for="type_0">Type</label>
        <select id="type_0" name="fields[0][type]">
          <option value="text">Text</option>
          <option value="number">Number</option>
          <option value="text-area">Text Area</option>
          <option value="select">Select</option>
          <option value="multi-select">Multi-Select</option>
          <option value="checkbox">Checkbox</option>
        </select> <br />
        <label for="require_0">Is Require</label>
        <input type="checkbox" id="require_0" name="fields[0][require]" /><br />
      </div>
    </div>

    <button type="button" onclick="addNewFieldDefinition()">+</button>

    <p>
      <hr />
      <input type="submit" value="Generate" />
    </p>
  </form>
  <script type="text/javascript" src="/generator/assets/js/index.js">
  </script>
</body>

</html>